// soal 1
console.log("----SOAL 1----")

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var soal1 = kataPertama + " " + (kataKedua[0].toUpperCase()+ kataKedua.slice(1))
            + " " + kataKetiga.slice(0,kataKetiga.length-1) + kataKetiga[kataKetiga.length-1].toUpperCase()
            + " " + kataKeempat.toUpperCase()

console.log(soal1)
console.log()

// soal 2
console.log("----SOAL 2----")

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang = 2 * (parseInt(panjangPersegiPanjang) + parseInt(lebarPersegiPanjang));
var luasSegitiga= 0.5 * parseInt(alasSegitiga) * parseInt(tinggiSegitiga);

console.log(kelilingPersegiPanjang)
console.log(luasSegitiga)

console.log()

//soal 3
console.log("----SOAL 3----")

var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substring(0, 3); 
var secondWord = sentences.substring(4,14)
var thirdWord= sentences.substring(15,18) 
var fourthWord= sentences.substring(19,24)
var fifthWord= sentences.substring(25,31) 

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

console.log()

//soal 4
console.log("----SOAL 4----")
var nilaiJohn = 80;
var nilaiDoe = 50;

if (nilaiJohn >= 80) {
  console.log("nilai John adalah A")
}else if(nilaiJohn >= 70 && nilaiJohn < 80){
  console.log("nilai John adalah B")
}else if(nilaiJohn >= 60 && nilaiJohn < 70){
  console.log("nilai John adalah C")
}else if(nilaiJohn >= 50 && nilaiJohn < 60){
  console.log("nilai John adalah D")
}else{
  console.log("nilai John adalah E")
}


if (nilaiDoe >= 80) {
  console.log("nilai Doe adalah A")
}else if(nilaiDoe >= 70 && nilaiDoe < 80){
  console.log("nilai Doe adalah B")
}else if(nilaiDoe >= 60 && nilaiDoe < 70){
  console.log("nilai Doe adalah C")
}else if(nilaiDoe >= 50 && nilaiDoe < 60){
  console.log("nilai Doe adalah D")
}else{
  console.log("nilai Doe adalah E")
}


console.log()

//soal 5
console.log("----SOAL 5----")

var tanggal = 22;
var bulan = 7;
var tahun = 2020;

var strBulan

switch (bulan){
  case 1:{ strBulan = "Januari"; break;}
  case 2:{ strBulan = "Februari"; break;}
  case 3:{ strBulan = "Maret"; break;}
  case 4:{ strBulan = "April"; break;}
  case 5:{ strBulan = "Mei"; break;}
  case 6:{ strBulan = "Juni"; break;}
  case 7:{ strBulan = "Juli"; break;}
  case 8:{ strBulan = "Agustus"; break;}
  case 9:{ strBulan = "September"; break;}
  case 10:{ strBulan = "Oktober"; break;}
  case 11:{ strBulan = "November"; break;}
  case 12:{ strBulan = "Desember"; break;}
  default:{ strBulan = "Undefined"; break;}
}

console.log(tanggal+ " " + strBulan + " " + tahun)