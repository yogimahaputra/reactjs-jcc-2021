//soal 1
console.log("----SOAL 1----")

console.log("LOOPING PERTAMA")
var iterationPertama = 1

while (iterationPertama <=20){
  if (iterationPertama % 2 === 0){
    console.log(iterationPertama + " - I love coding")
  }
  iterationPertama++
}

console.log("LOOPING KEDUA")
var iterationKedua = 20

while (iterationKedua > 0){
  console.log(iterationKedua + " - I will become a frontend developer")
  iterationKedua-=2
}

console.log()

//soal 2
console.log("----SOAL 2----")

for (var i=1; i<=20; i++){
  if (i % 3 === 0 && i % 2 === 1){
    console.log(i + " - I Love Coding")
  }else if(i % 2 === 1){
    console.log(i + " - Santai")
  }else{
    console.log(i + " - Berkualitas")
  }
}

console.log()

//soal 3
console.log("----SOAL 3----")


// cara pertama
// var iterationSegitiga = 1
// var pagar = ""

// while(iterationSegitiga <= 7){
//   pagar = pagar+ "#"
//   console.log(pagar)
//   iterationSegitiga++
// }

// cara kedua
for (var i=1; i<=7; i++){
  var pagar = ""
  for (var j=1; j<=i; j++){
    pagar = pagar + "#"
  }
  console.log(pagar)
}
console.log()


//soal 4
console.log("----SOAL 4----")

var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]

kalimat.shift()
kalimat.splice(2,1)

console.log(kalimat.join(" "))

console.log()


//soal 5
console.log("----SOAL 5----")

var sayuran=[]

sayuran.push("Kangkung")
sayuran.push("Bayam")
sayuran.push("Buncis")
sayuran.push("Kubis")
sayuran.push("Timun")
sayuran.push("Seledri")
sayuran.push("Tauge")
sayuran.sort()

var index = 0

while(index < sayuran.length){
  var number = index + 1
  console.log(number + ". " + sayuran[index])
  index++
}