// soal 1
console.log("----SOAL 1----")

function luasPersegiPanjang(p,l){
  return p*l
}

function kelilingPersegiPanjang(p,l){
  return 2*(p+l)
}

function volumeBalok(p,l,t){
  return p*l*t
}
 
var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

console.log()

// soal 2
console.log("----SOAL 2----")

function introduce(nama, umur, alamat, hobi){
  return "Nama saya "+ nama +", umur saya "+ umur + " tahun, alamat saya di "
  + alamat +", dan saya punya hobby yaitu "+ hobi +"!"
}
 
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) 

console.log()

// soal 3
console.log("----SOAL 3----")

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]

// cara pertama
var objectDataPeserta = {}
var arrayProperty = ["nama", "jenisKelamin", "hobi", "tahunLahir"]
arrayDaftarPeserta.forEach(function(value, index){
  objectDataPeserta[arrayProperty[index]] = value
})
console.log(objectDataPeserta)

// cara kedua
// var objDataPeserta = {
//   nama: arrayDaftarPeserta[0],
//   jenisKelamin: arrayDaftarPeserta[1],
//   hobi: arrayDaftarPeserta[2],
//   tahunLahir: arrayDaftarPeserta[3]
// }
// console.log(objDataPeserta)

console.log()


// soal 4
console.log("----SOAL 4----")

var dataBuah = [
  { nama: "Nanas", warna: "Kuning", adaBijinya: false,harga: 9000  }
  ,{nama: "Jeruk", warna: "Oranye", adaBijinya: true,harga: 8000 }
  ,{nama: "Semangka", warna: "Hijau & Merah", adaBijinya: true,harga: 10000 }
  ,{nama: "Pisang", warna: "Kuning", adaBijinya: false,harga: 5000 }
]

console.log(dataBuah.filter(function(item){
  return item.adaBijinya === false
}))

console.log()


// soal 5
console.log("----SOAL 5----")

function tambahDataFilm(nama, durasi, genre, tahun){
  var objFilm = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun
  }
  dataFilm.push(objFilm)
}

var dataFilm = []

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")
console.log(dataFilm)

console.log()
