// soal 1
console.log("----SOAL 1----")

const kelilingLingkaran = (r)=>{
  const pi = r % 7 === 0 ? 22/7 : 3.14
  return 2*pi*r
}

const luasLingkaran = (r)=>{
  const pi = r % 7 === 0 ? 22/7 : 3.14
  return pi*r*r
}

let jariJari = 7
console.log(kelilingLingkaran(jariJari))
console.log(luasLingkaran(jariJari))

console.log()

// soal 2
console.log("----SOAL 2----")

const introduce = (...params)=>{
  const [name, age, gender, profession] = params
  let prefixName = "Undefined"
  if (gender === "Laki-Laki"){
    prefixName = "Pak"
  }else if(gender === "Perempuan"){
    prefixName = "Bu"
  }
  return `${prefixName} ${name} adalah seorang ${profession} yang berusia ${age} tahun`
}
 
//kode di bawah ini jangan di rubah atau di hapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan) // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun" 

console.log()


// soal 3
console.log("----SOAL 3----")

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`)
    }
  }
}
  
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()

console.log()

// soal 4
console.log("----SOAL 4----")

let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
}

const {name: phoneName, brand: phoneBrand, year, colors} = phone
const [colorBronze, , colorBlack] = colors

console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 

console.log()


// soal 5
console.log("----SOAL 5----")

let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

buku = {
  ...buku, 
  ...dataBukuTambahan, 
  warnaSampul: [...buku.warnaSampul, ...warna]
}

console.log(buku)

console.log()