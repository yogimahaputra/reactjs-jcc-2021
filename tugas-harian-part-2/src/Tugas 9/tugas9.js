import React from "react"
import Logo from "../assets/img/logo-2.png"

const ListComponent = (props) => {
  return (
    <div class="input-checkbox">
      <input type="checkbox" /> <p>{props.listData}</p>
    </div>
  )
}

const Tugas9 = () => {
  return (
    <div className="card">
      <img src={Logo} />
      <p>THINGS TO DO</p>
      <small>During Bootcamp in Sanbercode</small>
      <hr />

      <ListComponent listData = "Belajar Git & CLI"/>
      <ListComponent listData = "Belajar HTML & CSS"/>
      <ListComponent listData = "Belajar Javascript"/>
      <ListComponent listData = "Belajar ReactJS Dasar"/>
      <ListComponent listData = "Belajar ReactJS Advance"/>

      <button>Send</button>
    </div>
  );
}

export default Tugas9;
