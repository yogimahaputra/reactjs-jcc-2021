import React, { useState, useEffect } from "react"
import axios from "axios"

const App = () => {
  let initialForm = {
    name: "",
    category: "",
    description: "",
    size: 0,
    price: 0,
    rating: 0,
    image_url: "",
    release_year: 2007,
    is_android_app: true,
    is_ios_app: true
  }

  const [mobileApps, setMobileApps] = useState(null)
  const [input, setInput] = useState(initialForm)
  const [selectedId, setSelectedId] = useState(null)
  const [fetchTrigger, setFetchTrigger] = useState(true)
  const [alertPlatform, setAlertPlatform] = useState(false)

  useEffect(() => {

    const fetchData = async () => {
      const  result = await axios.get(`https://www.backendexample.sanbersy.com/api/mobile-apps`)
      console.log(result.data)
      setMobileApps(
        result.data.map(el => {
          const { id, name, category, description, size, price, rating, image_url, release_year, is_android_app, is_ios_app } = el
          return {
            id,
            name,
            category,
            description,
            size,
            price,
            rating,
            image_url,
            release_year,
            is_android_app,
            is_ios_app
          }
        })
      )
    }

    if (fetchTrigger) {
      fetchData()
      setFetchTrigger(false)
    }


  }, [fetchTrigger])


  const handleChange = (event) => {
     let value = event.target.value
     let name = event.target.name
     let platform = ["is_android_app", "is_ios_app"] 

     if(platform.indexOf(name) < 0){
       setInput({...input, [name] : value})
     }else{
        setInput({...input, [name] : !input[name] })
     }
     
  }

  const handleSubmit = (event) => {

    event.preventDefault()
    console.log(input)
    if ((input.is_android_app === false && input.is_ios_app === false) || input.is_android_app === 0 && input.is_ios_app === 0) {
      setAlertPlatform(true)
    } else {
      if (selectedId === null) {
        console.log(input)
        axios.post(`https://www.backendexample.sanbersy.com/api/mobile-apps`, {
          name: input.name,
          category: input.category,
          description: input.description,
          size: parseInt(input.size),
          price: parseInt(input.price),
          rating: parseInt(input.rating),
          image_url: input.image_url,
          release_year: parseInt(input.release_year),
          is_android_app: input.is_android_app,
          is_ios_app: input.is_ios_app
        })
          .then(res => {
            setFetchTrigger(true)
          })
      } else {
        axios.put(`https://www.backendexample.sanbersy.com/api/mobile-apps/${selectedId}`, {
          name: input.name,
          category: input.category,
          description: input.description,
          size: parseInt(input.size),
          price: parseInt(input.price),
          rating: parseInt(input.rating),
          image_url: input.image_url,
          release_year: parseInt(input.release_year),
          is_android_app: input.is_android_app,
          is_ios_app: input.is_ios_app
        })
          .then(res => {
            setFetchTrigger(true)
          })
      }
      setSelectedId(null)
      setInput(initialForm)
      setAlertPlatform(false)
    }

  }

  const Action = ({ itemId }) => {
    const handleDelete = () => {

      axios.delete(`https://www.backendexample.sanbersy.com/api/mobile-apps/${itemId}`)
        .then(res => {
          setFetchTrigger(true)
        })

      if (selectedId === itemId) {
        setInput(initialForm)
      }
    }

    const handleEdit = async () => {
      let res = await axios.get(`https://www.backendexample.sanbersy.com/api/mobile-apps/${itemId}`)
      let singleMobileApp = res.data
      setInput({
        name: singleMobileApp.name,
        category: singleMobileApp.category,
        description: singleMobileApp.description,
        size: singleMobileApp.size,
        price: singleMobileApp.price,
        rating: singleMobileApp.rating,
        image_url: singleMobileApp.image_url,
        release_year: singleMobileApp.release_year,
        is_android_app: singleMobileApp.is_android_app,
        is_ios_app: singleMobileApp.is_ios_app
      })
      setSelectedId(itemId)
    }

    return (
      <>
        <button onClick={handleEdit}>Edit</button>
        &nbsp;
        <button onClick={handleDelete}>Delete</button>
      </>
    )
  }

  function truncateString(str, num) {
    if (str === undefined) {
      return ""
    } else {
      if (str === null) {
        return ""
      } else {
        if (str.length <= num) {
          return str
        }
        return str.slice(0, num) + '...'
      }
    }
  }


  return (
    <>
      <div>
      </div>

      <h1>Mobile Apps List</h1>
      <table style={{width:"100%"}}>
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Category</th>
            <th>Description</th>
            <th>Release Year</th>
            <th>Size</th>
            <th>Price</th>
            <th>Rating</th>
            <th>Platform</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

          {
            mobileApps !== null && mobileApps.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.name}</td>
                  <td>{item.category}</td>
                  <td title={item.description}>{truncateString(item.description, 20)}</td>
                  <td>{item.release_year}</td>
                  <td>{item.size}</td>
                  <td>{item.price}</td>
                  <td>{item.rating}</td>
                  <td> {item.is_android_app ? `Android` : ''} {item.is_ios_app ? `iOS` : ''}
                  </td>
                  <td>
                    <Action itemId={item.id} />

                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
      <br />
      {/* Form */}
      <h1>Mobile Apps Form</h1>
      <form style={{ textAlign: "left", paddingBottom: "100px" }} onSubmit={handleSubmit}>
        <div>
          <label style={{ display: "inline-block", width: "150px" }}>
            Name:
          </label>
          <input required style={{ display: "inline-block", width: "60%" }} type="text" name="name" value={input.name} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div>
          <label style={{ display: "inline-block", width: "150px" }}>
            Category:
          </label>
          <input required style={{ display: "inline-block", width: "60%" }} type="text" name="category" value={input.category} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div>
          <label style={{ display: "inline-block", width: "150px" }}>
            Description:
          </label>
          <textarea required style={{ display: "inline-block" }} cols="50" rows="3" type="text" name="description" value={input.description} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div style={{ marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Release Year:
          </label>
          <input required style={{ display: "inline-block" }} type="number" max={2021} min={2007} name="release_year" value={input.release_year} onChange={handleChange} />
          <br />
          <br />
        </div>



        <div style={{ marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Size(MB):
          </label>
          <input required style={{ display: "inline-block" }} type="number" name="size" value={input.size} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div style={{ marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Price:
          </label>
          <input required style={{ display: "inline-block" }} type="number" name="price" value={input.price} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div style={{ marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Rating:
          </label>
          <input required style={{ display: "inline-block" }} type="number" name="rating" min={0} max={5} value={input.rating} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div style={{ marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Image Url:
          </label>
          <textarea required style={{ display: "inline-block" }} cols="50" rows="3" type="text" name="image_url" value={input.image_url} onChange={handleChange} />
          <br />
          <br />
        </div>
        <div style={alertPlatform ? { marginTop: "20px", color: "red", border: "1px solid red" } : { marginTop: "20px" }}>
          <label style={{ display: "inline-block", width: "150px" }}>
            Platform:
          </label>
          <input style={{ display: "inline-block", cursor: "pointer" }} type="checkbox" name="is_android_app" checked={input.is_android_app} onChange={handleChange} />
          <strong>Android</strong>
          <br />
          <div style={{ display: "inline-block", width: "160px" }}>
          </div>
          <input style={{ display: "inline-block", cursor: "pointer" }} type="checkbox" name="is_ios_app" checked={input.is_ios_app} onChange={handleChange} />
          <strong>iOS</strong>
          {alertPlatform && <div>Please at least choose one</div>}
          <br />
        </div>
        <br />
        <br />
        <button style={{ padding: "10px", borderRadius: "10px", border: "1px solid #aaa", cursor: "pointer", fontWeight: "bold" }}>Submit</button>
      </form>
    </>
  )
}

export default App